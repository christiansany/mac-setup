# Mac Setup
## Base setup
* Login to your iClouds account from your new machine.
	* Open preferences/iCloud and add your iClouds account

### Install Apps from the AppStore
* Bear (also used for this guide)
* 1Password
* Dr. Cleaner
* Shade Control

### Install other Apps
* Spotify
* Google Chrome
* Firefox
* Opera
* Dropbox
* Spectacle
* Slack
* Sketch
* [Docker for Mac](https://docs.docker.com/docker-for-mac/install/)

### Mac preferences
#### General
* Enable Dark mode

#### Dock
* Set the size to 1/4
* Disable Animation when opening applications
* Enable automatically hide and show the Dock

#### Displays
* Set the resolution to scaled & more space

#### Keyboard
* Under ‘Text’, disable correction spelling automatically and capitalising words automatically. Disable Touch Bar typing suggestions.

#### Touch ID
* Set up your Touch ID
- Setup the usage of Touch ID for everything by enabling all the options

#### Accessibility
* Under ‘Display’, reduce both motion and transparency for

#### Finder (open Finder and then press cmd + ,)
* Make sure you show all file extensions
* Make sure you keep folders on top when sorting by name
* Lastly make sure that when you perform a search, you only search the folder you're in

### Clean your Dock from unnecessary stuff
* Remove…
	* Siri
	* Launchpad
	* Mail
	* Contacts
	* Calendar
	* Notes
	* Reminders
	* Maps
	* Photos
	* Facetime
	* iTunes
	* iBooks
	* AppStore
* Add… 
	* Bear
	* Google Chrome
	* Firefox
	* Opera
	* Spotify
	* Skype for Business
	* Slack
	* Applicationfolder

### Setup specific programs
#### 1Password
* Open 1Password and then choose iCloud

#### Google Chrome
* Make Chrome your default browser when first opening Chrome
* Sing in to your google account when first opening Chrome

#### Spectacle
* Enable all needed rights for Spectacle on first start of the app
* Enable starting spectacle on login by going to the app preferences and enabling the option there

#### Dropbox
* Set up your Dropbox account on first startup of the app

## Frontend development environment
* Install [iTerm2](https://www.iterm2.com/) from their Website
	* Go to the iTerm2 Preferences and change the following:
		* Under the Tab 'Apperance', change the Theme to 'Dark (High Contrast)'
		* Under the Tab 'Profiles', change the Working Directory to 'Reuse previous session's directory'
* Install [Node.js](https://nodejs.org/en/) from their Website
* Install zsh via `curl` (Command is on their [GitHub](https://github.com/robbyrussell/oh-my-zsh) page)
	* This will prompt you to install the Developer Tools from xcode, which will need anyway. So just install them, then and execute the `curl` again (if needed).
	* Install Spaceship theme for zsh using the `curl` command from their [GitHub](https://github.com/denysdovhan/spaceship-zsh-theme) page.
	* Update your zsh theme in your ~/.zshrc file using spaceship  
  ZSH_THEME="spaceship"
	* Restart zsh by executing `zsh` in your terminal (or iTerm)
* Install [Visual Studio Code](https://code.visualstudio.com/) from their website.
	* Install the package Settings Sync and then download the latest settings from your gist (Press Cmd + Shift + P and then enter ’sync download’ for the right command to pop up).
	* Execute the command > Install Code command in PATH
		* Now you can open vscode from any folder by typing `code .` in your terminal
* Install nvm using curl found on their [GitHub](https://github.com/creationix/nvm) page.
* Install [SourceTree](https://www.sourcetreeapp.com/) from their Website
	* Setup Sourcetree with your bitbucket account
* Install [Homebrew](https://brew.sh/) via instructions on their Website
* Install Graphics Magic with brew `brew install graphicsmagick`
* Generate an ssh key pair using this [Guide](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/)
	* Add your newly generates public key to wherevery you want (normally found in ~/.ssh/id_rsa.pub)

### Recommendations for globally installed node packages
* [npx](https://www.npmjs.com/package/npx)

### Add more stuff to your Dock
* iTerm
* SourceTree
* Visual Studio Code

It should look something like this:
TODO: Add new image with Opera
![alt text](./dock.png "Dock")

## Custom Hardware & associated drivers
### MX Master 2
* Install the driver from [MX Master - Logitech Support](http://support.logitech.com/en_us/product/mx-master/downloads)
